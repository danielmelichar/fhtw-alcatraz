# Alcatraz

- Florian Dienesch
- Marco Judt
- Daniel Melichar


## Development Environment

__Requirements__ 
- Linux machine (e.g. Debian, Ubuntu, Arch/Manjaro)
- JDK 1.8
- Wildfly Application Server 18.0.1
- Spread Toolkit (Version 4.0 [here]((http://edu.i4c.at/lab/vs/spread/spread-src-4.0.0.tar.gz)))

__Setup with IntelliJ__
1. Install the Spread Agent for the RegistryServer
    1. Download the Spread Sources and unpack them.
    3. Run the following commands, as stated Spread's documentaiton.
        ``` bash
        ./configure
       make
       make install
       ```
2. Start the Agent from this Project's directory and using the provided config.
   ``` bash
   spread -c spread.conf -n localhost 
   ```
3. Add the local libraries (```./libs/spread-4.0.0.jar``` and ```./libs/alcatraz-lib.jar```) into IntelliJ's Project Settings. 
4. You should now be able to start both examples in the respective Client and Server modules
    - directly in IntelliJ through a Run Configuration
    - in a CLI using the provided scripts in ```./bin/```
       
