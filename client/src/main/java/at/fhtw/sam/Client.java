package at.fhtw.sam;

import at.falb.games.alcatraz.api.*;
import at.fhtw.sam.domain.*;
import at.fhtw.sam.game.Move;
import at.fhtw.sam.util.Constants;
import at.fhtw.sam.util.MessagePanels;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;

/**
 * Client Controller.
 *
 * This implementation requires the name to be used in the game, as well as the IP address
 * and the port to be used for the RMI connection parameters of the object. We assume the validity of the last
 * two parameters and make apply no further methods of verification and create a unique object with which the other Client
 * runetimes can communicate, i.e. share moves, and start the game for everybody.
 *
 * The game starts after one Client has gotten verification from the RegistryServer. Afterwards, all Clients
 * get a list of the participating players, the Client-To-Client connections are started, and the initiating
 * Client forces everybody to start their game.
 */
public class Client extends UnicastRemoteObject implements IClient, MoveListener {

    /**
     * We use log4j for logging
     */
    private static final Logger logger = LogManager.getLogger(Client.class);

    /**
     * RegistryServer RMI stub
     */
    private IRegistryServer stub;

    /**
     * Client info
     */
    private int clientID;
    private int clientPort;
    private String clientIP;
    private String playername;

    /**
     * List of Client RMI objects as well as their game data
     */
    private List<ClientDTO> registeredPlayers;
    private List<IClient> clients;

    /**
     * The actual game. Provides methods to control the progress of the game.
     */
    private Alcatraz game;

    /**
     * Token with ID of the player whose turn it is and a counter if the current player does not respond.
     */
    private static int token = 0;
    private static int failedConnectCounter = 0;

    /**
     * Scheduled thread to check on the current player.
     */
    private static Timer timer;

    /**
     * Client Constructor
     * @param name to be used in-game
     * @param port the port on which the Client registry will be created
     * @param ip the IP on which the Client registry will be created
     * @throws RemoteException
     */
    Client(String name, int port, String ip) throws RemoteException {
        this.playername = name;
        this.clientPort = port;
        this.clientIP = ip;

        this.registeredPlayers = new ArrayList<>();
        this.clients = new ArrayList<>();
        this.game = new Alcatraz();

        this.initServerCommunication();
        this.startClientRegistry();
    }

    /**
     * Connect to the RegistryServer through RMI. We currently use pre-defined constants for this.
     * However, for non-development purpose, it is suggested to extend the Client with either an argument
     * or config parser to make this more dynamic.
     */
    @Override
    public void initServerCommunication() {
        try {
            this.stub = (IRegistryServer) Naming.lookup("rmi://"
                    .concat(Constants.RMI_HOSTNAME)
                    .concat(":")
                    .concat(Constants.RMI_PORT)
                    .concat("/")
                    .concat(Constants.RMI_SERVICE_SERVER));
            logger.info("Initialized server communication with " + this.stub);
        } catch (NotBoundException | MalformedURLException | RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create a RMI registry with the previously set port and with the service definition constant.
     */
    private void startClientRegistry() {
        try {
            Registry registry = LocateRegistry.createRegistry(this.clientPort);
            registry.rebind(Constants.RMI_SERVICE_CLIENT, this);
            logger.info("RMI registry created with port " + this.clientPort);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Register to the RegistryServer.
     * @throws RemoteException if problems happen with RMI, i.e. Server not available
     * @throws AlreadyRegisteredException if the previously set name is already set at the RegistryServer
     * @throws MalformedURLException if misconfigurations with the stub's URI occur
     * @throws NotBoundException if the RMI stub has not been set
     * @throws IllegalNameException if the previously set name does not comply with conventions
     */
    public void register() throws RemoteException, AlreadyRegisteredException, MalformedURLException, NotBoundException, IllegalNameException {
        logger.info("Asking server for registration");
        this.stub.register(this);
    }

    /**
     * Resign from the RegistryServer (a.k.a. sign-off, deregister, unregister).
     * @throws RemoteException if problems happen with RMI, i.e. Server not available
     */
    public void resign() throws RemoteException {
        logger.info("Asking server for resignation");
        this.stub.resign(this);
    }

    /**
     * Request the RegistryServer to start the game.
     * @throws RemoteException if problems happen with RMI, i.e. Server not available
     * @throws MalformedURLException if misconfigurations with the stub's URI occur
     * @throws NotBoundException if the RMI stub has not been set
     * @throws NotEnoughPlayersException if the minimum number of clients has not yet been reached
     */
    public void initGame() throws RemoteException, MalformedURLException, NotBoundException, NotEnoughPlayersException {
        logger.info("Game initialisation request received");
        this.stub.startGame();
    }

    /**
     * This method is called from the RegistryServer after it has verified the game initiation request.
     * It basically creates a data object from the received Client objects, connects to them, and then shows and
     * starts the actual Alcatraz game, but only for itself.
     * @throws RemoteException
     */
    @Override
    public void start() throws RemoteException {

        for (IClient c : this.clients) {
            if (c.getClientID() != this.getClientID()) {
                try {
                    /* Add all players to list */
                    this.registeredPlayers.add(new ClientDTO("rmi://"
                            .concat(c.getClientIP())
                            .concat(":")
                            .concat(c.getClientPort())
                            .concat("/")
                            .concat(Constants.RMI_SERVICE_CLIENT), c.getPlayername()));
                    logger.info("Remote player has been added to list " + registeredPlayers);
                } catch (NotBoundException | RemoteException e) {
                    e.printStackTrace();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        }

        /* Check if everyone is still here */
        for(ClientDTO c : this.registeredPlayers) {

            logger.info("Checking if player " + c.getName() + " is still alive.");
            boolean flag = c.isAlive();

            if(!flag) {
                /* Seems like we lost a player ... Good night */
                logger.info("Player " + c.getName() + " disconnected. Game cannot start.");
                c.exitInformation();
                this.abortAll();
            }
        }

        /* Init game */
        logger.info("Initialize Alcatraz Game with " + this.getClients().size() + " players.");
        this.game.init(this.getClients().size(), this.clientID);

        /* Set names */
        logger.info("Set name " + this.getPlayername() + " at ID: " + this.clientID);
        this.game.getPlayer(this.clientID).setName(this.getPlayername());
        this.registeredPlayers.forEach((player) -> {
            try {
                logger.info("Set name " + player.getName() + " at ID: " + player.getID());
                this.game.getPlayer(player.getID()).setName(player.getName());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        });

        /* Start actual game and show window with the game board */
        logger.info("Start game and show window with the game board.");
        this.game.showWindow();
        this.game.addMoveListener(this);
        this.game.start();

        /* Timer to check if the current player is still here */
        this.timer = new Timer();
        timer.schedule(new checkCurrentPlayer(), 0, Constants.TIMER_INTERVAL);
    }

    /**
     * Check if every client is still alive.
     *
     * @return
     */
    @Override
    public boolean isAlive() {
        return true;
    }

    /**
     *
     * @throws InterruptedException
     */
    public void currentIsAlive() throws Exception {

        /* If I'm the current exit */
        if (this.clientID == token) return;

        boolean flag;
        try {
            for (ClientDTO c : this.registeredPlayers) {
                if (c.getID() == token) {
                    /* Ping the current player */
                    c.isAlive();
                }
            }
        } catch (RemoteException e) {
            flag = false;
            while(!flag) {
                for (ClientDTO c : this.registeredPlayers) {
                    try {
                        if (c.getID() == token) {
                            break;
                        }
                    } catch (RemoteException ex) {
                        /* Ignore */
                    }
                    failedConnectCounter++;
                    if(failedConnectCounter > Constants.MAXIMUM_EXCEPTIONS) {
                        logger.info("Maximum attempts reached. Game will exit.");
                        MessagePanels.error("Maximum attempts reached. Game will exit.");
                        abortAll();
                    }
                    logger.info("Current player does not react. A new connection attempt is made.");
                    MessagePanels.warning("Current player does not react. A new connection attempt is made.");
                    Thread.sleep(Constants.TIMEOUT);
                    try {
                        if (c.getID() == token) {
                            /* Ping the current player */
                            flag = c.isAlive();
                            if(flag) {
                                failedConnectCounter = 0;
                            }
                        }
                    } catch (RemoteException ex) {
                        /* Ignore */
                    }
                }
            }
        }
    }

    /**
     *
     */
    private void changeCurrentPlayer() {
        token++;
        if(token > this.getClients().size() - 1) {
            token = 0;
        }
        logger.info("New current player ID is " + token);
    }

    /**
     * Does the actual move. Sets the Player and prisoner according to parameters. It moves the prisoner and does the
     * moves of other players received over the network.
     * @param clientID the client to move
     * @param prisonerID  the prisoner to move
     * @param rowOrCol weather to move col or row
     * @param row move row amount
     * @param col move col amount
     * @throws IllegalMoveException
     */
    @Override
    public void move(int clientID, int prisonerID, int rowOrCol, int row, int col) throws IllegalMoveException {
        this.game.doMove(this.game.getPlayer(clientID), this.game.getPrisoner(prisonerID), rowOrCol, row, col);
        changeCurrentPlayer();
    }

    /**
     * Implementation of MoveListener. If a Client has made a move, this method is called and asks the other Clients/Players to move as well.
     * @param player the played that moved
     * @param prisoner  the prisoner that moved
     * @param rowOrCol weather to move col or row
     * @param row move row amount
     * @param col move col amount
     */
    @Override
    public void moveDone(Player player, Prisoner prisoner, int rowOrCol, int row, int col) {
        if (player.getId() == this.clientID) {
            Move move = new Move(player, prisoner, rowOrCol, row, col);
            logger.info("moving " + prisoner + " to " + (rowOrCol == Alcatraz.ROW ? "row" : "col") + " " + (rowOrCol == Alcatraz.ROW ? row : col));

            for (ClientDTO c : this.registeredPlayers) {
                try {
                    triggerMoveForAllPlayers(c,move);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
            changeCurrentPlayer();
        }
    }

    /**
     * Trigger the move for all other players. If the move could not be sent to another player the client can decide
     * whether to try again or exit the game for all other players as well.
     * @param player
     * @param move
     */
    private void triggerMoveForAllPlayers(ClientDTO player, Move move) throws RemoteException {

        int exceptionCounter = 0;
        boolean success = false;

        while (!success && (exceptionCounter < Constants.MAXIMUM_EXCEPTIONS)) {
            try {
                /* Try move */
                logger.info("Try move for " + player.getName() + " (ID: " + player.getID() + ")");
                player.move(move.getMovedPlayer().getId(), move.getMovedPrisoner().getId(),move.getrowOrCol(),move.getrow(),move.getcol());
                logger.info("Move successful from " + player.getName() + " (ID: " + player.getID() + ")");

                /* All good */
                success = true;

            } catch (IllegalMoveException e) {
                e.printStackTrace();
            } catch (RemoteException e) {

                /* Client not reached */
                exceptionCounter++;

                /* Let client decide whether to try again or exit the game */
                try {
                    if (!this.clientNotReachedInformation(player)) {
                        break;
                    }
                    logger.info(player.getName() + " was not reached. A new connection attempt is made.");
                    Thread.sleep(Constants.TIMEOUT);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }

        /* Seems like we lost a player ... Good night */
        if (!success) {
            logger.info(player.getName() + " was not reached. The game will exit.");
                for (ClientDTO c : this.registeredPlayers) {
                    try{
                        c.exitInformation();
                    }
                    catch (RemoteException e) {
                        /* Ignore */
                    }
                }
            abortAll();
        }
    }

    /**
     * A player won, a notification is sent to everybody.
     * @param player
     */
    @Override
    public void gameWon(Player player) {
        logger.info("Player " + player.getName() + " (ID: " + player.getId() +") wins.");
    }

    /**
     * This method is called to resign from the RegistryServer and stop the game.
     * @throws Exception
     */
    @Override
    public void abort() throws Exception {
        logger.info("Shutdown request received");
        this.resign();
        if (this.game != null) {
            logger.info("Close and destroy game window");
            this.game.closeWindow();
            this.game.disposeWindow();
        }
        System.exit(0);
    }

    /**
     * This method is called if anything happens during the Game start. It basically asks every
     * Client to resign from the RegistryServer and abort the game.
     */
    private void abortAll() {
        for (ClientDTO client : this.registeredPlayers) {
            try {
                client.abort();
            } catch (Exception e) {
                /* Ignore */
            }
        }
        try {
            this.abort();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Trigger player information window for every client to show who is in the game.
     *
     * @throws RemoteException
     */
    @Override
    public void playersInGame() throws RemoteException {
        for (ClientDTO c : this.registeredPlayers) {
            c.playersInGame();
        }
    }

    /**
     * Show the players in the game.
     */
    @Override
    public void showPlayersInGameWindow() {
        MessagePanels.info("The game was started! Players in the game: " + getRegisteredPlayers());
    }

    /**
     * Show warning message if a client was not reached after a move was tried to send. Let player decide to try again or exit the game.
     * @param client
     * @return
     */
    private boolean clientNotReachedInformation(ClientDTO client) {
        Object[] options = {"Try again", "Exit game"};
        int n = MessagePanels.optionDialog("Player " + client.getName() + " is not responding.", options);
        if (n == 0) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Show an information that the game is about to exit.
     */
    @Override
    public void exitInformation() {
        MessagePanels.error("A Player left the game. The game ends now.");
    }


    /* ------------------------------ GETTER AND SETTER ------------------------------ */

    public int getPlayerNumber() throws RemoteException {
        return this.stub.getClientNumber();
    }

    @Override
    public String getPlayername() {
        return this.playername;
    }

    @Override
    public int getClientID() {
        return this.clientID;
    }

    @Override
    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    @Override
    public List<IClient> getClients() {
        return clients;
    }

    @Override
    public void setClients(List<IClient> clients) {
        this.clients = clients;
    }

    @Override
    public String getClientIP() {
        return clientIP;
    }

    @Override
    public String getClientPort() {
        return Integer.toString(this.clientPort);
    }

    @Override
    public List<String> getRegisteredPlayers() {
        return registeredPlayers.stream().map(ClientDTO::getName).collect(Collectors.toList());
    }

    class checkCurrentPlayer extends TimerTask {

        @Override
        public void run() {
            try {
                currentIsAlive();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}



