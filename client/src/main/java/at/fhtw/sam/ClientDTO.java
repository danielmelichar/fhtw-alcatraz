package at.fhtw.sam;

import at.falb.games.alcatraz.api.IllegalMoveException;
import at.fhtw.sam.domain.IClient;

import javax.swing.*;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * This class holds a serializable representation of the Client and the named used separately.
 */
public class ClientDTO implements Serializable {
    
    private IClient client;
    private String name;

    public ClientDTO(String ip, String name) throws RemoteException, NotBoundException, MalformedURLException {
        this.client = (IClient) Naming.lookup(ip);
        this.name = name;
    }

    public void abort() throws Exception {
        client.abort();
    }

    public boolean isAlive() throws RemoteException {
        return client.isAlive();
    }

    public void move(int clientID, int prisonerID, int rowOrCol, int row, int col) throws IllegalMoveException, RemoteException {
        client.move(clientID, prisonerID, rowOrCol, row, col);
    }

   public void playersInGame() throws RemoteException {
        client.showPlayersInGameWindow();
    }

    public void exitInformation() throws RemoteException {
        client.exitInformation();
    }

    /* ------------------------------ GETTER ------------------------------ */

    public String getName() {
        return name;
    }

    public int getID() throws RemoteException {
        return client.getClientID();
    }
}
