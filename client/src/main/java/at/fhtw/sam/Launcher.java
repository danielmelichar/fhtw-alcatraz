package at.fhtw.sam;

import at.fhtw.sam.domain.AlreadyRegisteredException;
import at.fhtw.sam.domain.IllegalNameException;
import at.fhtw.sam.domain.NotEnoughPlayersException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import static at.fhtw.sam.util.MessagePanels.error;
import static at.fhtw.sam.util.MessagePanels.info;

/**
 * This Launcher implementation uses Swing for the registration using a provided name, to resign from the Server,
 * and to start the actual Alcatraz game. In theory, it could be replaced with any other user interface, e.g. CLI
 * or JavaFX. We have used IntelliJ's GUI Design tool to create the interface and bound this class to it.
 */
public class Launcher extends JFrame implements ActionListener {

    /**
     * Swing Components
     */
    private JPanel mainPanel;
    private JPanel connectionPanel;
    private JPanel buttonPanel;
    private JLabel playerLabel;
    private JButton registerButton;
    private JButton startButton;
    private JButton resignButton;
    private JTextField playerName;

    /**
     * Client controller for functionality
     */
    private Client client;

    /**
     * Launcher Constructor
     */
    public Launcher() {
        super();

        /* Controller set through registration */
        this.client = null;

        /* Initialize the Swing components */
        this.createUIComponents();
        this.setContentPane(mainPanel);

        /* If the user decides to exit, the Client will also resign */
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                try {
                    if (client != null) client.abort();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        /* Further swing config */
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.pack();
        this.setResizable(false);
    }

    /**
     * Creates Swing components and adds ActionListeners to the buttons.
     */
    private void createUIComponents() {
        this.registerButton = new JButton();
        this.startButton = new JButton();
        this.resignButton= new JButton();

        this.registerButton.addActionListener(this);
        this.startButton.addActionListener(this);
        this.resignButton.addActionListener(this);
    }


    /**
     * Either show or disable this Window
     * @param v
     */
    public void open(boolean v) {
        this.setVisible(v);
    }

    /**
     * The operations/actions that happen on each button's press.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String name = this.playerName.getText();

        if ( name.isEmpty() ) {
            error("Please enter a valid player name.");
        } else {
            if (e.getActionCommand().equals("Register")) {
                this.register(name);
            }
            if (e.getActionCommand().equals("Start")) {
                this.start();
            }
            if (e.getActionCommand().equals("Resign")) {
                this.resign();
            }
        }

        this.validate();
        this.repaint();
    }

    /**
     * Register to the RegistryServer using the name provided in the textfield. We also get the IP address
     * of the machine on which this Launcher has been started.
     * @param name the name from the text field
     */
    public void register(String name) {

        try {
            String ip = InetAddress.getLocalHost().getHostAddress();
            if (this.client == null) this.client = new Client(name, findFreePort(), ip);
            this.client.register();

            boolean gameStarted = false;
            if (client.getPlayerNumber() == 0) {
                info("Successfully registered [" + name + "] to server. Maximum players reached - The game is starting shortly.");
                gameStarted = true;
            }
            else {
                info("Successfully registered [" + name + "] to server. Currently waiting players: [" + client.getPlayerNumber() + "]");
            }
            if (gameStarted) {
                this.client.playersInGame();
            }
        } catch (RemoteException | NotBoundException | UnknownHostException e) {
            error("Error: could not connect to remote server");
            e.printStackTrace();
        } catch (MalformedURLException e) {
            error("Error: the URI you entered is invalid");
            e.printStackTrace();
        } catch (AlreadyRegisteredException e) {
            error("Error: you have already registered");
            e.printStackTrace();
        } catch (IllegalNameException e) {
            error("Error: Playername is already taken");
            this.client = null;
            e.printStackTrace();
        }
    }

    /**
     * Resign from the server.
     */
    public void resign() {
        try{
            if (this.client == null) {
                error("Please register first");
            } else {
                this.client.resign();
                this.client = null;
                info("Successfully resigned from server.");
            }
        } catch (RemoteException e) {
            error("Error: could not resign from remote server");
            e.printStackTrace();
        }
    }

    /**
     * Here we basically ask the RegistryServer if we can start the game, and if so, show who we are playing with.
     */
    public void start()  {
        try {
            if (this.client == null) {
                error("Please register first.");
            }
            else {
                this.client.initGame();
                info("You [" + this.client.getPlayername() + "] are starting the game with " + this.client.getRegisteredPlayers());
                this.client.playersInGame();
            }
        } catch (NotEnoughPlayersException x) {
            error("Not enough players have registered.");
        } catch (RemoteException | NotBoundException | MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Helper function to get free port on this machine.
     * @return a free port
     */
    private static int findFreePort() {
        try (ServerSocket socket = new ServerSocket(0)) {
            socket.setReuseAddress(false);
            int port = socket.getLocalPort();
            try {
                socket.close();
            } catch (IOException e) {
                /* Ignore IOException on close() */
            }
            return port;
        } catch (IOException ignored) {
        }
        return 0;
    }

    /**
     * Create the GUI and show it. For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public static void main(String[] args) {
        /* Schedule a job for the event-dispatching thread:
        creating and showing this application's GUI. */
        EventQueue.invokeLater( () -> new Launcher().open(true) );
    }
}