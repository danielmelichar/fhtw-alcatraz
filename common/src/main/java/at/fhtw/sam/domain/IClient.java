package at.fhtw.sam.domain;


import at.falb.games.alcatraz.api.IllegalMoveException;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Client stub
 */
public interface IClient extends Remote {

    void move(int clientId, int prisonerId, int rowOrCol, int row, int col) throws RemoteException, IllegalMoveException;
    void start() throws RemoteException, MalformedURLException, NotBoundException;
    void abort() throws Exception;
    void initServerCommunication() throws RemoteException;
    boolean isAlive() throws RemoteException;
    void playersInGame() throws RemoteException;
    void showPlayersInGameWindow() throws RemoteException;
    void exitInformation() throws RemoteException;

    /* ------------------------------ GETTER AND SETTER ------------------------------ */
    String getPlayername() throws RemoteException;
    void setClientID(int clientID) throws RemoteException;
    int getClientID() throws RemoteException;
    List<IClient> getClients() throws RemoteException;
    void setClients(List<IClient> clients) throws RemoteException;
    String getClientIP() throws RemoteException;
    String getClientPort() throws RemoteException;
    List<String> getRegisteredPlayers() throws RemoteException;
}
