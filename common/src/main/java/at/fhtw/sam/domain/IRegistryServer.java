package at.fhtw.sam.domain;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Server stub
 */
public interface IRegistryServer extends Remote {

    void register(IClient client) throws RemoteException, AlreadyRegisteredException, MalformedURLException, NotBoundException, IllegalNameException;
    void resign(IClient client) throws RemoteException;
    void startGame() throws RemoteException, MalformedURLException, NotBoundException, NotEnoughPlayersException;
    int getClientNumber() throws RemoteException;
}
