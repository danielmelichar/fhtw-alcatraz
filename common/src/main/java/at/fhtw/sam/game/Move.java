package at.fhtw.sam.game;

import at.falb.games.alcatraz.api.Player;
import at.falb.games.alcatraz.api.Prisoner;

import java.io.Serializable;

/**
 * Serializable wrapper for a game move
 */
public class Move implements Serializable {

    private Player movedPlayer;
    private Prisoner movedPrisoner;
    private int rowOrCol;
    private int row;
    private int col;

    public Move(Player movedPlayer, Prisoner movedPrisoner, int rowOrCol, int row, int col) {
        this.movedPlayer = movedPlayer;
        this.movedPrisoner = movedPrisoner;
        this.rowOrCol = rowOrCol;
        this.row = row;
        this.col = col;
    }

    public Player getMovedPlayer() {
        return movedPlayer;
    }

    public Prisoner getMovedPrisoner() {
        return movedPrisoner;
    }

    public int getrowOrCol() {
        return rowOrCol;
    }

    public int getrow() {
        return row;
    }

    public int getcol() {
        return col;
    }

}
