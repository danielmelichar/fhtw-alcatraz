package at.fhtw.sam.util;

/**
 * Configuration parameters.
 */
public class Constants {

    /**
     * Game
     */
    public final static Integer MAXIMUM_PLAYERS     = 4;
    public final static Integer MINIMUM_PLAYERS     = 2;

    /**
     * RMI for Client to RegistryServer
     */
    public final static String RMI_HOSTNAME         = "localhost";
    public final static String RMI_SERVICE_SERVER   = "RegistryServer";
    public final static String RMI_PORT             = "1213";

    /**
     * RMI for Client to client
     */
    public final static String RMI_SERVICE_CLIENT   = "PlayerClient";
    public final static Integer MAXIMUM_EXCEPTIONS  = 10;
    public final static Integer TIMEOUT             = 3000;
    public final static Integer TIMER_INTERVAL      = 20000;

    /**
     * Spread
     */
    public final static String  SPREAD_HOST          = "localhost";
    public final static Integer SPREAD_PORT          = 0;
    public final static String  SPREAD_GROUPNAME     = "groupName";

    /**
     * States
     */
    public enum SERVER_STATE {PRIAMRY, REPLICA}
}
