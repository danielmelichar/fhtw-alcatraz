package at.fhtw.sam.util;

import javax.swing.*;

/**
 * Swing helper panels
 */
public class MessagePanels {

    public static void error(String msg) {
        JOptionPane.showMessageDialog(null, msg, "Alcatraz", JOptionPane.ERROR_MESSAGE);
    }

    public static void warning(String msg) {
        JOptionPane.showMessageDialog(null, msg, "Alcatraz", JOptionPane.WARNING_MESSAGE);
    }

    public static int optionDialog(String msg, Object[] options) {
        return JOptionPane.showOptionDialog(null, msg, "Alcatraz", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[1]);
    }

    public static void info(String msg) {
        JOptionPane.showMessageDialog(null, msg, "Alcatraz", JOptionPane.INFORMATION_MESSAGE);
    }
}
