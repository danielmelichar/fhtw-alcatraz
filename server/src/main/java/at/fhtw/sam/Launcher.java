package at.fhtw.sam;

import at.fhtw.sam.domain.IRegistryServer;
import at.fhtw.sam.util.Constants;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Make the Services provided by the RegistryServer available through RMI.
 *  For now, we are using the Constants defined common. In the future this will be replaced with an
 *  Argument parser like Apache Commons to interactively define the desired network setup
 */
public class Launcher {

    public static void main(String[] args) {
        try {
            RegistryServer server = new RegistryServer(Constants.SPREAD_HOST, Constants.SPREAD_PORT, Constants.SPREAD_GROUPNAME);
            IRegistryServer stub = (IRegistryServer) UnicastRemoteObject.exportObject(server, Integer.parseInt(Constants.RMI_PORT));
            Registry registry = LocateRegistry.createRegistry(Integer.parseInt(Constants.RMI_PORT));
            registry.rebind(Constants.RMI_SERVICE_SERVER, stub);

        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}