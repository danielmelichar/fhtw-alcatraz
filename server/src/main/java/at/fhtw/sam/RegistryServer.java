package at.fhtw.sam;

import at.fhtw.sam.domain.*;
import at.fhtw.sam.util.Constants;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import spread.*;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import static at.fhtw.sam.util.Constants.MAXIMUM_PLAYERS;
import static at.fhtw.sam.util.Constants.SERVER_STATE;

/**
 * The RegistryServer manages the connected Clients. It allows Clients to register and resign, and also
 * verifies the game start. If enough players have successfully registered, it informs the connected Clients/Players
 * of their fellow Clients/Players and then starts the game for everybody.
 *
 * Through the usage of Spread we can also ensure that no single-point of failure can happen since the Server is
 * replicated. The replicas have to be defined in the in the spread.conf and also in (our case) the Constants.
 * We use a ring-like algorithm to elect the primary instance if an server joins the group, leaves it, or times out.
 */
public class RegistryServer implements IRegistryServer, AdvancedMessageListener {

    /**
     * For logging
     */
    private static final Logger logger = LogManager.getLogger(RegistryServer.class);

    /**
     * All connected clients
     */
    private List<IClient> players;

    /**
     * Instance state: primary, replica
     */
    private SERVER_STATE state;

    /**
     * Spread interfaces
     */
    private SpreadConnection con;
    private SpreadGroup group;

    /**
     * registryServer Constructor
     * @param host the host on which to listen to
     * @param port the port on which to listen to
     * @param groupName the spread which to join
     */
    RegistryServer(String host, int port, String groupName) {
        players = new ArrayList<>();

        this.initSpread(host, port, groupName);
    }

    /**
     * Adds a Client to the register list if it that has not been done so yet.
     * @param client the client to add
     * @throws AlreadyRegisteredException client has already been added
     * @throws RemoteException error during RMI
     * @throws IllegalNameException name is not valid
     */
    @Override
    public void register(IClient client) throws AlreadyRegisteredException, RemoteException, IllegalNameException {
        logger.info("New Client registration. " + client);

        /* If the object is already added */
        if (players.contains(client)) {
            logger.info("Client has already registered. " + client, new AlreadyRegisteredException());
            throw new AlreadyRegisteredException();
        }

        /* If the name is already added */
        for (IClient player : players) {
            if (player.getPlayername().equals(client.getPlayername())) {
                logger.info("Playername already exists. " + client, new IllegalNameException());
                throw new IllegalNameException();
            }
        }

        /* Actually add to list of registered clients */
        this.players.add(client);
        logger.info("Client has been added to list " +  players);

        /* If the maximum has been reached now, start the game. */
        if (players.size() == MAXIMUM_PLAYERS) {
            logger.info("Maximum number of clients reached, starting game.");
            try {
                this.startGame();
            } catch (NotEnoughPlayersException e) {
                e.printStackTrace();
            }
        }

        /* Finally send the list of clients to other replicas. */
        logger.info("Updating RegistryServer Replicas with new clientlist");
        sendMulticast(new RegistryServerDTO(players));
    }

    /**
     * Removes a client from the RegistryServer(s)
     * @param client the client to be removed
     * @throws RemoteException
     */
    @Override
    public void resign(IClient client)  throws RemoteException {
        logger.info("Resignation received: " + client);

        /* Remove the object from the list */
        players.remove(client);

        /* Double check if the player's name is also removed */
        players.forEach(c -> {
            try {
                c.getPlayername().equals(client.getPlayername());
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                /* Ignore */
            }
        });

        /* Update replicas with new list */
        logger.info("Updating RegistryServerReplicas with new clientlist");
        sendMulticast(new RegistryServerDTO(players));
    }

    /**
     * Check if a game start is possible, and do so if that is the case.
     * @throws RemoteException
     * @throws NotEnoughPlayersException
     */
    @Override
    public void startGame() throws RemoteException, NotEnoughPlayersException {
        logger.info("A request to start the game has been received. Currently there are" + players.size() + " players");

        for (IClient c : players) {
            logger.info("Setting list of players at " + c + " to " + players);
            c.setClients(players);
            logger.info("Setting client id of client " + c + " to " + players.indexOf(c));
            c.setClientID(players.indexOf(c));
        }

        if (players.size() < Constants.MINIMUM_PLAYERS) {
            logger.info("The minimum number of players has not been reached. " + players, new NotEnoughPlayersException());
            throw new NotEnoughPlayersException();
        } else {

            /* We operate on an array here since a Client could resign during the process and cause an concurrency exception. */
            IClient[] playerArray = players.toArray(new IClient[players.size()]);
            for (IClient client : playerArray) {
                try {
                    logger.info("Starting game for client. " + client);
                    client.start();
                } catch (MalformedURLException | NotBoundException e) {
                    e.printStackTrace();
                }
            }
            /* After all have started, we flush the our players list and thereby allow new connections */
            for (IClient client : playerArray) {
                logger.info("Resigining client from list. " + client);
                this.resign(client);
            }
        }
    }

    /**
     * The number of currently registered players
     * @return registered players
     * @throws RemoteException
     */
    @Override
    public int getClientNumber() throws RemoteException {
        logger.info("Received current request for current number of clients " + this.players.size());
        return this.players.size();
    }


    /* ------------------------------ SPREAD ------------------------------ */

    /**
     * Connects to spread daemon and joins the spread group
     * @param host of the spread daemon
     * @param port of the spread daemon
     * @param groupName group to join
     */
    private void initSpread(String host, int port, String groupName) {
        con = new SpreadConnection();
        group = new SpreadGroup();

        try {
            con.connect(InetAddress.getByName(host), port, groupName, false, true);
            group.join(con, groupName);
            con.add(this); /* Add Server as MessageListener */
        } catch (SpreadException | UnknownHostException e) {
            e.printStackTrace();
        }
    }

    /**
     * Leaves Spread group and disconnects
     */
    private void stopSpread() {
        try {
            group.leave();
            con.remove(this);
        } catch (SpreadException e) {
            e.printStackTrace();
        }
    }

    /**
     * Used to send the currently registered players list.
     * @param dto the list of current players
     */
    private void sendMulticast(RegistryServerDTO dto) {
        SpreadMessage message = new SpreadMessage();
        message.addGroup(Constants.SPREAD_GROUPNAME);
        message.setSafe();
        // message.setReliable();
        // message.setType(Constants.MSG_TYPE_REG);
        try {
            message.setObject(dto);
            con.multicast(message);
        } catch (SpreadException e) {
            e.printStackTrace();
        }
    }

    /**
     * Spread message handler used to update this instance's list of players
     * @param spreadMessage
     */
    @Override
    public void regularMessageReceived(SpreadMessage spreadMessage) {
        /* For updates on game state through the DTO get message and update values here */

        if (spreadMessage.isSafe()) {
            logger.info("#SPREAD# - Received regular safe message from: " + spreadMessage.getSender());
            try {
                players = (List<IClient>) ((RegistryServerDTO) spreadMessage.getObject()).getPlayers();

                logger.info("#SPREAD# - Userlist updated.");

            } catch (SpreadException e) {
                e.printStackTrace();
            }
        }
        else {
            logger.info("#SPREAD# - Received regular message from: " + spreadMessage.getSender());
        }
    }

    /**
     * Spread message handler used for primary election
     * @param spreadMessage
     */
    @Override
    public void membershipMessageReceived(SpreadMessage spreadMessage) {
        logger.info("#SPREAD# - membershipMessageReceived called");

        MembershipInfo info = spreadMessage.getMembershipInfo();

        if (info.isCausedByJoin()) {
            logger.info("#SPREAD# - A member joined: " + info.getJoined());

            if (info.getMembers().length == 1) {
                state = SERVER_STATE.PRIAMRY;
                logger.info("#SPREAD# - Im the primary now");
            } else {
                state = SERVER_STATE.REPLICA;
            }

            if (state == SERVER_STATE.PRIAMRY) {
                sendMulticast(new RegistryServerDTO(players));
                logger.info("#SPREAD# - DTO sent");
            }

        } else if(info.isCausedByLeave() || info.isCausedByDisconnect() || info.isCausedByNetwork() || info.isSelfLeave()) {
            logger.info("#SPREAD# - Members changed, starting votePrimary ");
            votePrimary(spreadMessage);
        }
    }

    /**
     * Election protocol in ring-like fashion
     * @param msg
     */
    private void votePrimary(SpreadMessage msg) {
        if (msg.getMembershipInfo().getMembers()[0].equals(con.getPrivateGroup())) {
            if (state.equals(SERVER_STATE.REPLICA)) {
                /* Set to primary */
                state = SERVER_STATE.PRIAMRY;

                players.forEach(c -> {
                    try {
                        c.initServerCommunication();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                });
            }
        } else {
            /* not first member, replica */
            state = SERVER_STATE.REPLICA;
        }
    }
}