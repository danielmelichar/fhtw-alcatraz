package at.fhtw.sam;

import at.fhtw.sam.domain.IClient;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Serializable object of current players. Used for updates with Spread messages.
 */
public class RegistryServerDTO implements Serializable {

    private List<IClient> players;

    public RegistryServerDTO(List<IClient> players) {
        this.players = players;
    }

    public List<IClient> getPlayers() {
        return players;
    }
}
